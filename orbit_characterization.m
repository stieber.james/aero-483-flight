clear
close all 

mu_sun = 1.3271244e11; % km^3 s^-2
mu_earth = 3.986e5; % km^3 s^-2
AU = 1.49597870e8; % km
v_earth = sqrt(mu_sun/AU);


% .................................
% set radii of orbits here (in AU)

R1 = 1; % leave this at 1 (earth)
R2 = 1;

% .................................



r1 = R1*AU;
x1 = cosspace(-r1,r1);
y1p = r1*sqrt(1-(x1/r1).^2);
y1m = -r1*sqrt(1-(x1/r1).^2);

r2 = R2*AU;
x2 = cosspace(-r2,r2);
y2p = r2*sqrt(1-(x2/r2).^2);
y2m = -r2*sqrt(1-(x2/r2).^2);


s2d = (3600*24)^-1;
tH = s2d*pi*sqrt(((r1+r2).^3)/(8*mu_sun)); % in days

% plots
% figure(), hold on
% scatter(0,0,20,'filled','g');
% plot(x1,y1p,'k',x1,y1m,'k');
% plot(x2,y2p,'b',x2,y2m,'b');
% axis equal


% Geosynchronous Transfer orbit
i = 28.5; % degrees, Kennedy
r_earth = 6378; % km
h_LEO = 250; % km 
h_GEO = 35786; % km
rp = r_earth+h_LEO;
ra = r_earth+h_GEO;

v_LEO = sqrt(mu_earth/rp); % km/s
dv_gt1 = sqrt(mu_earth*(2/rp-2/(rp+ra)))-v_LEO; % km/s

% 3 burns
vta = sqrt(mu_earth*(2/ra-2/(rp+ra)));
v_GEO = sqrt(mu_earth/ra);
dv_gt2 = v_GEO-vta;
dv_ic = 2*sqrt(mu_earth/ra)*sind(i/2);
dv_T3 = dv_gt1+dv_gt2+dv_ic;

% 3 burns
dv_gtic = sqrt(vta^2+v_GEO^2-2*vta*v_GEO*cosd(i));
dv_T2 = dv_gt1+dv_gtic;

% Escape burns
h_LEO = 250; % km 
h_GEO = 35786; % km

% straight from LEO
vesc_LEO = sqrt(2*mu_earth/(r_earth+h_LEO));
dv1 = vesc_LEO-v_LEO;

% LEO to GEO to escape
vesc_GEO = sqrt(2*mu_earth/(r_earth+h_GEO));
dv2 = dv_gt1+dv_gt2+(vesc_GEO-v_GEO);

% LEO to smaller elliptical orbit to escape
h = linspace(15,h_LEO);
rsub = r_earth+h;
dv3 = abs(sqrt(mu_earth*(2/rp-2./(rp+rsub)))-v_LEO)+(vesc_LEO-sqrt(mu_earth*(2./rsub-2./(rp+rsub))));
plot(h,dv3);