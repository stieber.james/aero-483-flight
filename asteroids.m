clear, close all

% ASTEROID APPROXIMATE DENSITY
rho = 2 * (1/1000) * (100000/1)^3; % avg density, kg/km^3

% GRAVITATIONAL CONSTANT
G = 6.674e-20; % G, km^3 kg^-1 s^-2

% SET ASTEROID DIAMETERS
d1 = 20; % asteroid spherical diameter, km
d2 = 60; 
d3 = 100;
d4 = 140;
% -------------------------------


M1 = pi/6*d1^3*rho;
mu1 = G*M1;
M2 = pi/6*d2^3*rho;
mu2 = G*M2;
M3 = pi/6*d3^3*rho;
mu3 = G*M3;
M4 = pi/6*d4^3*rho;
mu4 = G*M4;


% SPHERE OF INFLUENCE
AU = 1.49597870e8; % 1 AU in km
a = 2.06*AU; % approximate semi-major axis of asteroid (to Sun)
Msun = 1.989e30; % mass of Sun, kg
rSOI1 = a*(M1/Msun)^(2/5);
rSOI2 = a*(M2/Msun)^(2/5);
rSOI3 = a*(M3/Msun)^(2/5);
rSOI4 = a*(M4/Msun)^(2/5);

% MINIMUM ORBITING ALTITUDE
min_alt = 100;

r1 = linspace(d1/2+min_alt,rSOI1);
v1 = sqrt(mu1./r1);
r2 = linspace(d2/2+min_alt,rSOI2);
v2 = sqrt(mu2./r2);
r3 = linspace(d3/2+min_alt,rSOI3);
v3 = sqrt(mu3./r3);
r4 = linspace(d4/2+min_alt,rSOI4);
v4 = sqrt(mu4./r4);

plot(r1-d1/2,v1,r2-d2/2,v2,r3-d3/2,v3,r4-d4/2,v4,'Linewidth',2);
xlabel('Orbit Altitude (km)');
ylabel('Circular Orbit Velocity (km/s)');
legend('d = 20 km','d = 60 km','d = 100 km','d = 140 km');




