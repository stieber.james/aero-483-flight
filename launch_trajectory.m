clear, close all

deg2rad = pi/180;
alpha = -1*deg2rad; % rads
T = 7500; % Newtons
Isp = 320; % seconds
m0 = 400; % kg
x0 = [6500,1000000,0];
tf = 10;
[t,x] = ode45(@(t,x) traj_dyn(t,x,alpha,T,m0,Isp), [0,tf], x0);

% PLOTS
figure()
plot(t,x(:,1));

figure()
plot(t,x(:,2));

figure()
plot(t,x(:,3));


function dxdt = traj_dyn(t,x,alpha,T,m0,Isp)
dxdt = zeros(3,1);
r = x(1); v = x(2); gamma = x(3);
mu = 3.986e14;
g = 9.807;
Toverm = (T/m0)/(1-(T/m0)*(t/(g*Isp)));
dxdt(1) = v*sin(gamma);
dxdt(2) = Toverm*(cos(alpha))/v-(mu/r^2)*sin(gamma);
dxdt(3) = Toverm*(sin(alpha))/v+(v/r-mu/(v*r^2))*cos(gamma);
end