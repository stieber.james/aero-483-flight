% Lambert solver

clear, close all

AU= 149597870; % astronomical unit in km
muC= 1.327124e11; % Sun grav. parameter in km^3/s^2
day2sec = 3600*24; % convert num days to seconds

% ////////////////////////////////////////////////////////////
% DON'T CHANGE ANYTHING ABOVE THIS


% input starting and ending sun-centric locations

% Earth position on AUG 4 2038
X = 1.000288155265125E+08; Y =-1.047517849705135E+08; Z =-4.540591740692367E+07;
VX= 2.192470536288030E+01; VY= 1.791969928593472E+01; VZ= 7.766711669465704E+00;
Vearth = [VX,VY,VZ]; %*AU/day2sec;
r1vec = [X,Y,Z]; %*AU

% Apophis position on AUG 4 2038
X =-1.230607846157513E+08; Y =-7.288310344467631E+07; Z =-3.061279421410843E+07;
VX= 2.104448548712116E+01; VY=-2.231126035218418E+01; VZ=-8.402327242406409E+00;
Vast0 = [VX,VY,VZ]; %*AU/day2sec;
rast0 = [X,Y,Z]; %*AU

% Earth position on MAR 16 2040
X =-1.483431915348822E+08; Y = 1.080910608907085E+07; Z = 4.687024701695454E+06;
rearthfinal = [X,Y,Z];

% Apophis position on MAR 16 2040
X = 1.475639030624984E+08; Y = 8.591641240803024E+07; Z = 3.612720439269416E+07;
VX=-9.961203039143793E+00; VY= 2.320095743807351E+01; VZ= 8.934023835167721E+00;
Vast = [VX,VY,VZ]; %*AU/day2sec;
r2vec = [X,Y,Z]; %*AU

% input transfer time
tf= 590; % 190 days

% input number of orbits completed by target during transfer
m= 1.0; % num orbits


% DO NOT CHANGE
[V1, V2, extremal_distances] = lambert(r1vec, r2vec, tf, m, muC);

% departure
Vinf_dep = V1-Vearth;
dV_dep = norm(Vinf_dep);
C3_dep = dV_dep^2;
dla = atan2d(Vinf_dep(3),norm(Vinf_dep(1:2)));
RA = atan2d(Vinf_dep(2), Vinf_dep(1));

% arrival
Vinf_arr = V2-Vast;
dV_arr = norm(Vinf_arr);
C3_arr = dV_arr^2;
phase_angle = acosd(dot(Vinf_arr/norm(Vinf_arr), r2vec/norm(r2vec)));
app_angle = acosd(dot(Vinf_arr/norm(Vinf_arr), Vast/norm(Vast)));

% total
EA_range = norm(r2vec-rearthfinal)/AU; % Earth-asteroid range
ES_range = norm(rearthfinal)/AU; % Earth-Sun range
AS_range = norm(r2vec)/AU; % Asteroid-Sun range
SEP_angle = acosd((EA_range^2+ES_range^2-AS_range^2)/(2*EA_range*ES_range));
dV = dV_dep+dV_arr;

fprintf("Departure:\n");
fprintf("V-inf = %2.2f km/s\n",dV_dep);
fprintf("C3 = %2.3f km^2/s^2\n",C3_dep);
fprintf("Declination Launch Asymptote = %2.2f degrees\n",dla);
fprintf("RA Launch Asymptote = %2.2f degrees\n",RA);
fprintf("\n");

fprintf("Arrival:\n");
fprintf("V-inf = %2.2f km/s\n",dV_arr);
fprintf("Phase angle = %2.2f degrees\n",phase_angle);
fprintf("Approach angle = %2.2f degrees\n",app_angle);
fprintf("Sun-Earth-Probe angle = %2.2f degrees\n",SEP_angle);
fprintf("\n");

fprintf("Earth to Asteroid Range = %2.3f AU\n",EA_range);
fprintf("Total Delta-V = %2.2f km/s\n",dV);

% ////////////////////////////////////////////////////////////
% PLOTTING

% input number of simulation steps
N = 1000;

% DO NOT CHANGE
% ////////////////////////////////////////////////////////////
r0 = r1vec;
v0 = V1;
% ////////////////////////////////////////////////////////////

% don't change first two arguments

% input third argument: equal to final time in seconds
% input fourth argument: equal to # of steps in simluation OR equal to time step
% follow this by 4th arg specifier, either "timestep" or "numsteps"
[r,v] = position(r0,v0,tf*day2sec,N,"numsteps");
[re,ve] = position(r0,Vearth,tf*day2sec,N,"numsteps");
[ra,va] = position(rast0,Vast0,tf*day2sec,N,"numsteps");

% DO NOT CHANGE
% ////////////////////////////////////////////////////////////
% r_error = norm(r2vec'-r(:,end));
% fprintf("Norm of error in final radius is %6.3f\n",r_error);
% v_error = norm(V2'-v(:,end));
% fprintf("Norm of error in final velocity is %6.3f\n",v_error);
% ////////////////////////////////////////////////////////////


figure(), hold on
plot3(r(1,:)/AU,r(2,:)/AU,r(3,:)/AU,'--')
plot3(re(1,:)/AU,re(2,:)/AU,re(3,:)/AU)
plot3(ra(1,:)/AU,ra(2,:)/AU,ra(3,:)/AU)
scatter3(r1vec(1)/AU,r1vec(2)/AU,r1vec(3)/AU,60,'*b','LineWidth',1);
scatter3(r2vec(1)/AU,r2vec(2)/AU,r2vec(3)/AU,60,'*k','LineWidth',1);
legend('Transfer','Earth','Apophis','Departure','Arrival');
xlabel('X (AU)');
ylabel('Y (AU)');
zlabel('Z (AU)');
grid on
axis equal