Isp = 320; % s
g0 = 9.807; % m/s^2
dV = 1000:10000; % m/s
m0 = 500; % kg
mf = m0*exp(-(dV/(Isp*g0)));
mfuel = m0-mf;
%fprintf("Initial Mass: %4.1f kg\n", m0);
%fprintf("Fuel Mass Required: %4.1f kg\n", mfuel);

figure()
plot(dV,mfuel)