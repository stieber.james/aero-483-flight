clear
close all 

mu_sun = 1.3271244e11; % km^3 s^-2
mu_earth = 3.986e5; % km^3 s^-2
AU = 1.49597870e8; % km
s2y = (3600*24*365.2569)^-1;


% .................................
% set radii of orbits here (in AU)

R1 = 1; % leave this at 1 (earth)
R2p = 0.5; % must be less than 1
R2a = 9; % must be greater than 1

% .................................

e = (R2a-R2p)/(R2a+R2p); % between 0 and 1
r1 = R1*AU;
x1 = cosspace(-r1,r1);
y1p = r1*sqrt(1-(x1/r1).^2);
y1m = -r1*sqrt(1-(x1/r1).^2);

a = (R2p+R2a)/2*AU;
b = a*sqrt(1-e^2);
d = a-R2p*AU;
x2 = cosspace(-a+d,a+d);
y2p = b*sqrt(1-((x2-d)/a).^2);
y2m = -b*sqrt(1-((x2-d)/a).^2);

% plots
figure(), hold on
scatter(0,0,20,'filled','g');
plot(x1,y1p,'k',x1,y1m,'k');
plot(x2,y2p,'k',x2,y2m,'k');
axis equal

T_earth = s2y*2*pi*sqrt((r1^3)/mu_sun);
T_neo = s2y*2*pi*sqrt((a^3)/mu_sun);


a5yr = (mu_sun*(5/(pi*s2y))^2)^(1/3); % half period of 5 years

%rpmax = r1;
%e = 