close all
locations = ["KSC","Vand","CSG"];

for i = 1:length(locations)
    
    if locations(i) == "KSC"
        % Kennedy Space Center
        lat = 28.5; % degrees
        title_str = 'Kennedy Space Center';

        azi_min = 35; % degrees
        azi_max = 120; % degrees
        
    elseif locations(i) == "Vand"
        % Vandenberg Air Force Base
        lat = 34.7; % degrees
        title_str = 'Vandenberg Air Force Base';

        azi_min = 158; % degrees
        azi_max = 244; % degrees
        
    elseif locations(i) == "CSG"
        % Guiana Space Centre
        lat = 5.2; % degrees
        title_str = 'Guiana Space Centre';

        azi_min = 0; % degrees
        azi_max = 95; % degrees

    end
    
    azi = linspace(azi_min,azi_max,500);
    inc = acosd(cosd(lat)*sind(azi));
    [mininc,minind] = min(inc);
    minazi = azi(minind);
    [maxinc,maxind] = max(inc);
    maxazi = azi(maxind);

    figure(), hold on
    plot(azi,inc,'linewidth',2);
    scatter(minazi,mininc,100,'*','Linewidth',1.5)
    scatter(maxazi,maxinc,100,'*','Linewidth',1.5)
    legmin = ['Minimum Inclination = ', num2str(round(mininc,2)),'\circ'];
    legmax = ['Maximum Inclination = ',num2str(round(maxinc,2)),'\circ'];
    legend('Range',legmin,legmax)
    grid on
    xlabel('Fixed Inertial Launch Azimuth');
    ylabel('Inclination of Orbit');
    title(title_str);
end