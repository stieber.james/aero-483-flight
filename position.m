function [r,v] = position(rinit,vinit,T,var,varopt)

if varopt == "timestep"
    dT = var;
    N = ceil(T/dT);
elseif varopt == "numsteps"
    N = round(var);
    if N < 1
        error('N must be greater than or equal to 1.');
    end
    dT = T/var;
else
    error('Fifth function argument must be "timestep" or "numsteps".');
end

r0 = [rinit(1);rinit(2);rinit(3)];
v0 = [vinit(1);vinit(2);vinit(3)];

u = zeros(6,N+1);
un = [r0(1);r0(2);r0(3);v0(1);v0(2);v0(3)];
u(:,1) = un;
for k = 1:N
    
    % RK4
    k1 = dT*f(un);
    k2 = dT*f(un+k1/2);
    k3 = dT*f(un+k2/2);
    k4 = dT*f(un+k3);
    unp = un+(k1+2*k2+2*k3+k4)/6;
    u(:,k+1) = unp;
    un = unp;
    
end

r = u(1:3,:);
v = u(4:6,:);


end

function unp = f(un)
unp = [un(4);un(5);un(6);0;0;0];
%G = 6.674e-20; % m^3*kg^-1*s^-2
R = sqrt((un(1))^2+(un(2))^2+(un(3))^2);
mu_sun = 1.3271244e11;
unp(4) = -mu_sun*un(1)/R^3;
unp(5) = -mu_sun*un(2)/R^3;
unp(6) = -mu_sun*un(3)/R^3;
end